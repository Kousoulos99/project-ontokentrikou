public class RequestDonation{
    Entity entity;
    double quantity;
    int ID;
    public RequestDonation (Entity e,double q) {
        entity=e;
        quantity=q;
        
        
    }
    
    void updateQuantity(double newQuantity){
        quantity+=newQuantity;    
    }
    
    void setQuantity(double q){
         quantity=q;   
    }
    
    double getQuantity(){
         return quantity;   
    }
    
    void setID(){
        String[] temp=entity.getEntityInfo().split(" ");
        ID=Integer.parseInt(temp[0]);        
    }
    
    int getID(){
        return ID;
    }
    
    void print(){
        System.out.println(entity.getName()+ ": "+quantity);
    }
    
}