import java.util.ArrayList;
public class Organization
{
    
    String name;
    Admin admin;
    RequestDonationList currentDonations;
    ArrayList <Entity> entityList = new ArrayList<Entity>();
    ArrayList <Donator> donatorList = new ArrayList<Donator>();
    ArrayList <Beneficiary> beneficiaryList = new ArrayList<Beneficiary>();
    
    void setAdmin(){
        admin.setadmin(true);
    }
    
    void donatorCommit(int i){
        donatorList.get(i).commit();
    }
    
    boolean getAdmin(){
        return admin.getadmin();
    }
    
    void removeEntity(int i){
         if(admin.getadmin())
             entityList.remove(i);
    }
    
    void addEntity(int ID,String name,String desc){
        Entity temp = new Entity(ID,name,desc);
        entityList.add(temp);
           
    }
    
    void addDonation(int ID,double posotita,String desc){
        currentDonations.add(ID,posotita,desc);
    }
    
    
    void insertDonator(String onoma,String tilefono){
       Donator temps = new Donator(onoma,tilefono);
        donatorList.add(temps);
    }
    
    void removeDonator(int y){
         donatorList.remove(y);
    }
    
    public Donator getDonator(int x){return donatorList.get(x);}
    
    public int getDonatorSize(){return donatorList.size();}
    
    void insertBeneficiary( String onoma,String tilefono,int people){
            Beneficiary temp = new Beneficiary(onoma,tilefono,people);
        beneficiaryList.add(temp);
        
    }
    
    void removeBeneficiary(int y){
         beneficiaryList.remove(y);
    }
     
    public Beneficiary getBeneficiary(int x){return beneficiaryList.get(x);}
    
    public int getBeneficiarySize(){return beneficiaryList.size();}
    
    void listEntities(){
        showMaterial();
        showServices();       
    }
    
    void showOffers(int i){
        
    }
    
    void showMaterial(){
        System.out.println("Material: ");
        int countFound=0;
        for(int i=0;i<entityList.size();i++){
            if(entityList.get(i).getDescription().equals("Material")){
                countFound++;
                System.out.println(countFound + ". "+entityList.get(i).getEntityInfo());
            }
        }
    }
    
    void showServices(){
        System.out.println("Services: ");
        int countFound=0;
        for(int i=0;i<entityList.size();i++){
            if(entityList.get(i).getDescription().equals("Service")){
                countFound++;
                System.out.println(countFound + ". "+entityList.get(i).getEntityInfo());
            }
        }
    }
    
    void listBeneficiaries(){
        System.out.println("Beneficiaries: ");
        for(int i=0;i<beneficiaryList.size();i++){
                System.out.println(i+1 +". "+beneficiaryList.get(i).getDetails());
            }
        }
     
 
}
