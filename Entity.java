public class Entity
{
   String name, description;
   int id;
   
   public Entity(int ID, String onoma, String desc){
       id=ID;
       name=onoma;
       description=desc;
    }
   
   String getEntityInfo(){
        return String.valueOf(id) +" "+ name; 
    }
    
    public String toString(){
        return getEntityInfo()/*+" "+getDetails()*/;
    }
    
    String getName(){
        return name;
        
    }
    
    String getDescription(){
        return description;
    }
    }
