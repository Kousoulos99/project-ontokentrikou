import java.util.Scanner;
public class Main{
     public static void showAdminMenu(Organization organ, Admin adm){
         int epilogi;
         int epilogi2;
         System.out.println("Geia sou "+adm.getName()+" eisai diaxirstis kai to tilefono sou einai: "+adm.getNumber());

         Scanner in = new Scanner(System.in);
         do{
         System.out.println("1. View\n 2. Monitor Organization\n 3. Back\n 4. Logout\n 5. Exit");
         epilogi=in.nextInt();
         switch(epilogi){
             case 1:
             do{
                 System.out.println("1. Material\n 2. Services");
                 epilogi2=in.nextInt();
                 
                 switch(epilogi2){
                     case 1:
                     organ.showMaterial();
                     break;
                     
                     case 2:
                     organ.showServices();
                     break;
                     
                     default:
                     System.out.println("Edoses lathos epilogi, Dose ksana");
                 }
             }while(!((epilogi2>=1)&&(epilogi2<=2)));
             break;
             
             case 2:
             /*do{
             System.out.println("1. Material\n 2. Services");
             epilogi2=in.nextInt();
            }while( */
            
            case 3:
            showAdminMenu(organ,adm);
            break;
            
            case 5:
            System.exit(0);
         }
        }while(!((epilogi>=1) && (epilogi<=5)));
     }
     
     public static void showDonatorMenu(Organization organ, int thesi){
         int epilogi,epilogi2,id;
         char epilogi3;
         double quantity;
         Scanner in = new Scanner(System.in);
         
         do{
         System.out.println("1. Add offer\n 2. Show offers \n 3. Commit \n 4. Back \n 5. Logout \n 6. Exit");
         epilogi=in.nextInt();
         
         switch(epilogi){
             case 1:
             do{
                 System.out.println("1. Material\n 2. Services");
                 epilogi2=in.nextInt();
                 
                 switch(epilogi2){
                     case 1:
                     organ.showMaterial();
                     System.out.println("Thelete na prosferete? (Y/N)");
                     epilogi3=in.next().charAt(0);
                     if(epilogi3=='Y'){
                         System.out.println("Dose ID pou theleis na prosferis: ");
                         id=in.nextInt();
                         System.out.println("Dose posotita pou tha prosferis: ");
                         quantity=in.nextDouble();
                         organ.addDonation(id,quantity,"Material");
                     }
                     break;
                     
                     case 2:
                     organ.showServices();
                     System.out.println("Thelete na prosferete? (Y/N)");
                     epilogi3=in.next().charAt(0);
                     if(epilogi3=='Y'){
                         System.out.println("Dose ID pou theleis na prosferis: ");
                         id=in.nextInt();
                         System.out.println("Dose posotita pou tha prosferis: ");
                         quantity=in.nextDouble();
                         organ.addDonation(id,quantity,"Service");
                     }
                     break;
                     
                     default:
                     System.out.println("Edoses lathos epilogi, Dose ksana");
                 }
             }while(!((epilogi2>=1)&&(epilogi2<=2)));
             break;
             
             case 3:
             Donator temp = organ.getDonator(thesi);
             temp.commit();
             break;
             
             
             case 4:
             showDonatorMenu(organ,thesi);
             break;
             
             case 6:
             System.exit(0);
             break;
             
         }
         
        }while(!((epilogi>=1) && (epilogi<=6)));
     }
     
     public static void showBeneficiaryMenu(Organization organ, int thesi){
         int epilogi,epilogi2;
         do{
         Scanner in = new Scanner(System.in);
         System.out.println("1. Add Request\n 2. Show Requests \n 3. Commit \n 4. Back \n 5. Logout \n 6. Exit");
         epilogi=in.nextInt();
         switch(epilogi){
             case 1:
             System.out.println("1. Material\n 2. Services");
                 epilogi2=in.nextInt();
                 
                 switch(epilogi2){
                     case 1:
                     organ.showMaterial();
                     
                     break;
                     
                     case 2:
                     organ.showServices();
                     
                     break;
                     
                     default:
                     System.out.println("Edoses lathos epilogi, Dose ksana");
                 }
                 
                 case 4:
                 showDonatorMenu(organ,thesi);
                 break;
                 
                 case 6:
                 System.exit(0);
                 break;
            
            }
         
      }while(!((epilogi>=1) && (epilogi<=6)));
    }
     
     public static void main(String[] args) {
    Organization org =new Organization();
    org.addEntity(1,"milk","Material");
    org.addEntity(2,"sugar","Material");
    org.addEntity(3,"rice","Material");
    org.addEntity(4,"MedicalSupport","Service");
    org.addEntity(5,"NurserySupport","Service");
    org.addEntity(6,"BabySitting","Service");
    
    Admin diaxiristis=new Admin ("Lampros","99999999");
    org.insertBeneficiary("Antreas","88888888",2);
    org.insertBeneficiary("Kostas","77777777",7);
    org.insertDonator("xristos","55555555");
    
   
    Scanner in=new Scanner(System.in);
    String tilefono;
    tilefono=in.nextLine();
    boolean found=false;
    
    char epilogi;
    int eidos,atoma;
    String onoma,til;
    
    if (tilefono.equals(diaxiristis.getNumber())){
        found=true;
        showAdminMenu(org,diaxiristis);
    }
    else{
        int i=0 ;
        while( i<org.getBeneficiarySize() && !found){
            Beneficiary temp=org.getBeneficiary(i);
            if(tilefono.equals(temp.getNumber())){
                found=true;
                System.out.println("Geia sou " +temp.getName()+" o organismos mas einai edw gia na sas voithisi.");
                showBeneficiaryMenu(org,i);
                
            }
            i++;
        }
        i=0;
        while( i<org.getDonatorSize() && !found){
            Donator temp=org.getDonator(i);
            if(tilefono.equals(temp.getNumber())){
                found=true;
                System.out.println("Geia sou " +temp.getName()+" efxaristoume pou prosfereis ston organismo mas.");
                showDonatorMenu(org,i);
            }
            i++;
        }
        
        if (!found){
            System.out.println("o Arithmos pou dosate den antistixi se eggegrameno xristi");
            System.out.println("Thelete na graftite? Y/N");
            epilogi=in.next().charAt(0);
            if(epilogi=='Y'){
                do{//elegxoume an exi dosi sosto arithmo 
                    System.out.println("ti tha itheles na eggrafis :");
                    System.out.println("1.Beneficiary");
                    System.out.println("2.Donator");
                    eidos=in.nextInt();
                }while ((eidos != 1) && (eidos!=2));
                switch(eidos){
                    case 1:
                    //exi epile3i na grafti os beneficiary
                    System.out.println("Dose onoma: ");
                    onoma=in.next();
                    
                    System.out.println("Dose arithmo atomwn: ");
                    atoma=in.nextInt();
                    
                    org.insertBeneficiary(onoma,tilefono,atoma); 
                    showBeneficiaryMenu(org,i+1);
                    
                    break;
                    
                    case 2://exi epile3i na grafti os Donator
                    System.out.println("Dose onoma: ");
                    onoma=in.next();
                    
                    org.insertDonator(onoma,tilefono);                
                    showDonatorMenu(org,i+1);
                    
                    break;
                
            }
           
            
        }
    }
}

}
}
