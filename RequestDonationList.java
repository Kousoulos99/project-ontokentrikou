import java.util.ArrayList;
public class RequestDonationList{
    ArrayList<RequestDonation> rdEntities = new ArrayList<RequestDonation>();
    String temp;
    
    int get(int Id){
        int i;
        for (i=0;i<rdEntities.size();i++){
            if (rdEntities.get(i).getID()==Id)
            return i;
        }
        return -1;
    }
    
    void add (int id, double posotita, String desc){
            int i=isUnique(id);
            if(i!=-1){
                rdEntities.get(i).updateQuantity(posotita);
            }
            else{
                Entity temps = new Entity(id,"",desc);
                RequestDonation temp = new RequestDonation(temps,posotita);
                rdEntities.add(temp);
            }
    }
    
    int isUnique(int ID){
        for (int i=0;i<rdEntities.size();i++){
            if (rdEntities.get(i).getID()==ID)
                return i;
        }
        return -1;
    }
    
    void remove (int i){
        rdEntities.remove(i);
    }
    
    void modify(double posotita,int i){
        rdEntities.get(i).setQuantity(posotita);  
    }
    
    void monitor(){
        int i;
        for (i=0;i<=rdEntities.size();i++)
        rdEntities.get(i).print();   
    }
    
    void reset(){
        rdEntities.clear();
    }
    
    
}
