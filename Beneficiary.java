public class Beneficiary extends User {
    int noPersons=1;
    RequestDonationList receivedList;
    Requests requestsList;
    public Beneficiary(String onoma,String tilefono,int people){
        super(onoma,tilefono);
        noPersons=people;
    }
    
    public String getDetails(){
        return super.getName()+" "+super.getNumber()+" "+"Atoma: "+this.noPersons;    
    }
    
}
