public class Material extends Entity {
    double level1=0,level2=0,level3=0;
    int people;
    public Material (int ppl){
        super(0,"","");
        people=ppl;
        setLevel(ppl);
    }
    
    int getPeople(){
        return people;
    }
    
    String getDetails(){
        return Integer.toString(getLevel());
    }
    
    void setLevel(int people){
        if(people == 1)
            level1=1;
        else if(people>=2 && people <=4)
            level2=1;
        else if(people>=5)
            level3=1;
    }
    
    int getLevel(){
        if(level1==1)
            return 1;
        else if(level2==1)
            return 2;
        else if(level3==1)
            return 3;
            
        return -1;
    }
    
}
